<?php

namespace App\Http\Controllers;

use App\Good;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;



use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class GoodsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home')->with('goods',Good::all());
    }


    public function submitGood(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'Quantity' => 'required',
            'Price' => 'required',

        ]);

        if ($validator->fails()) {
            return response()->json(array('success' => false,'errors' => $validator->messages()));
        } else {
            $name = Input::get('name');
            $Quantity = Input::get('Quantity');
            $Price = Input::get('Price');



            DB::table('goods')->insert(
                array(
                    'name' => $name,
                    'Quantity' => $Quantity,
                    'Price' => $Price,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                )
            );
        }
    }


    public function goodsView(){
        if (\Auth::guest()) {
            return view('index');
        } else {

            $goods = DB::table('goods')->get();




            return Response::json(array(
                'success' => true,
                'data' => $goods
            ));
        }
    }

}