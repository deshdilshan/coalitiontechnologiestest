<!DOCTYPE html>
<html>
  <head>
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" >

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  </head>
  <body>
    <form>
      <div class="form-group">
       <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <label for="exampleInputEmail1">Product name</label>
        <input type="text" class="form-control" id="name" aria-describedby="" placeholder="">

      </div>

      <div class="form-group">
        <label for="exampleInputEmail1">Quantity in stock</label>
        <input type="number" class="form-control" id="Quantity" aria-describedby="" placeholder="">

      </div>

      <div class="form-group">
        <label for="exampleInputEmail1">Price per item</label>
        <input type="number" class="form-control" id="Price" aria-describedby="" placeholder="">

      </div>

     <button type="submit" class="btn btn-primary" onclick="submitGood()">Submit</button>

    </form>

    <br><br>
    <div class="detailsTable" id="detailsTable">
        <table id="goodsTable" class="table table-bordered table-striped">
            <thead>
            <tr>
            <th>name</th>
            <th>Quantity</th>
            <th >price</th>
            </tr>
            </thead>
            <tbody>
            @foreach($goods as $good)

                <tr>
                        <td >{{$good->name}}</td>
                        <td >{{$good->quantity}}</td>
                        <td >{{$good->price}}</td>
                </tr>
             @endforeach
          </tbody>
        </table>

    </div>


    <script>

         function submitGood() {

                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')},
                    url: "submitGood",
                    type: "POST",
                    data: {
                        name: $('#name').val(),
                        Quantity: $('#Quantity').val(),
                        Price: $('#Price').val()
                    },
                    success: function (response) {
                        if (response.success == false) {
                            $('#err').empty();
                            var length = Object.keys(response.errors).length;
                            var layout = '';
                            if (length > 0) {
                                layout = '<div class="alert alert-danger">' +
                                        '<strong>Whoops!</strong> There were some problems with your input.<br><br>' +
                                        '<ul>';
                                Object.keys(response.errors).forEach(function (key) {
                                    layout += '<li>' + response.errors[key][0] + '</li>';
                                });
                                '</ul>' +
                                '</div>'
                                $('#err').append(layout);
                            }
                        } else {
                            $('#err').empty();
                            layout = '<div class="alert alert-success">' +
                                    '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                                    'Successfully Inserted<br><div>'
                            $('#err').append(layout);
                            $('#name').val('');
                            $('#Quantity').val('');
                            $('#Price').val('');

                            goodsView();
                        }
                    },
                    error: function (e) {

                    }
                });
            }

             function returnPaymentView() {
                    $.ajax({
                        url: "viewGoods",
                        type: "GET",
                        success: function (response) {
                            $('#goodsTable').empty();
                            var length = response.data.length;
                            var attendance_mode;
                            var payment_mode;
                            var layout;
                            layout = '<table id="goodsTable" class="table table-bordered table-striped">' +
                                    '<thead>' +
                                    '<tr>' +
                                    '<th>name</th>' +
                                    '<th>Quantity</th>' +
                                    '<th >price</th>' +
                                    '</tr>' +
                                    '</thead>' +
                                    '<tbody>';
                            for (var i = 0; i < length; i++) {

                                layout += '<tr>' +
                                        '<td >' + response.data[i].name + '</td>' +
                                        '<td >' + response.data[i].quantity + '</td>' +
                                        '<td >' + response.data[i].price + '</td>' +
                                        '</tr>';
                            }
                            layout += '</tbody>' +
                                    '</table>';

                            $('#detailsTable').append(layout);
                            $(document).ready(function () {
                                $('#detailsTable').DataTable();
                            });
                        },
                        error: function (e) {

                        }
                    });
                }
    </script>
    <script src="http://code.jquery.com/jquery.js"></script>

  </body>
</html>